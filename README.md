# Treestand

A tool similar to [watchdower](https://github.com/containrrr/watchtower) for k8s.
Watch deployments for an update in their image and then trigger an update.

This way the respective deployment can be updated from within the cluster.
This project is NOT YET PRODUCTION READY.

## Workflow

Each `entry` you specify will start a independent workflow which will execute the following steps:
1. Filter for all k8s resources based on a filter (e.g. type and label)
2. Define the image to watch based on a jsonpath configuration
3. Check if the remote image is newer than the installed on. If so:
   a) Patch the resource based on a configuration (not available yet)
   b) trigger an update with the configured strategy
4. repeat

## Configuration

Lets assume you got a `statefulset` like this, which points to an image tag that updates every day and you use `imagePullPolicy`: `IfNotPresent`.
By default k8s won't update your the underlying image when it updates without you triggering it.
```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  creationTimestamp: "2021-05-24T18:02:37Z"
  generation: 1
  labels:
    app.kubernetes.io/name: veloren
  name: veloren-gameserver
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: veloren
  serviceName: veloren-gameserver
  template:
    metadata:
      labels:
        app: server
    spec:
      containers:
      - envFrom:
        image: registry.gitlab.com/veloren/veloren/server-cli:nightly
        imagePullPolicy: IfNotPresent
  updateStrategy:
    rollingUpdate:
      partition: 0
    type: RollingUpdate
```

Default configuration to be mounted `/etc/treestand/config.ron`
```ron
(
    entries: [
        (
            name: "example1"
            username: "test",
            resourceFilter: ResourceFilter(
                kind: "statefulset",
                selector: None,
                field_selector: Some("metadata.name=veloren-gameserver"),
            ),
            jsonpath: "{.spec.template.spec.containers[*].image}",
            check_delay: (
                secs: 60,
                nanos: 0,
            ),
            update_delay: (
                secs: 30,
                nanos: 0,
            ),
            delay_after_rollout: (
                secs: 300,
                nanos: 0,
            ),
            verifyStrategy: Time,
            updateStrategy: Rollout,
        ),
    ],
)
```

For a detailed list of possible options, have a look at the `src/settings.rs` file