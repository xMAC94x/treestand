use clap::Parser;

#[derive(Parser)]
#[command(
    name = "Treestand",
    about = "Allows you to restart k8s Deployments when their image updates",
    author = "Marcel Märtens <marcel.cochem@googlemail.com>"
)]
pub struct Opts {
    /// Sets a custom config file.
    #[arg(short, long, default_value = "/opt/treestand/config/settings.ron")]
    pub config: String,
    /// A level of verbosity, and can be used multiple times
    #[arg(short, long, action = clap::ArgAction::Count)]
    pub verbose: u8,
    /// Instead of running as a daemon forever, when this flag is activated
    /// every check is only applied once sequentially
    #[arg(long)]
    pub run_once: bool,
    /// Skip the initial health test which verifies kubectl, network and
    /// settings. Instead directly start with the production code.
    #[arg(long)]
    pub skip_self_check: bool,
    /// Don't run a https server for readiness and prometheus metrics endpoint.
    #[arg(long)]
    pub no_server: bool,
    /// Server port to listen on
    #[arg(long, default_value = "8081")]
    pub port: u16,
}
