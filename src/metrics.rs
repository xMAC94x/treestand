use prometheus::{IntCounter, IntCounterVec, Opts, Registry};

#[derive(Debug, Clone)]
pub struct ResourceMetrics {
    pub checks_started: IntCounterVec,
    pub checks_finished: IntCounterVec,
    pub verify_image_checks: IntCounterVec,
    pub verify_image_results: IntCounterVec,
    pub update_calls: IntCounterVec,
    pub kubeapi_calls: IntCounter,
    pub registry_requests: IntCounterVec,
}

impl ResourceMetrics {
    pub fn new(registry: &Registry) -> Result<Self, prometheus::Error> {
        let checks_started = IntCounterVec::new(
            Opts::new(
                "checks_started_total",
                "number of checks started per resource",
            ),
            &["name"],
        )?;
        let checks_finished = IntCounterVec::new(
            Opts::new(
                "checks_finished_total",
                "number of checks finished per resource, with an indication if they succeeded or \
                 failed",
            ),
            &["name", "succeeded"],
        )?;
        let verify_image_checks = IntCounterVec::new(
            Opts::new(
                "verify_image_checks_total",
                "number of image verified per resource",
            ),
            &["name"],
        )?;
        let verify_image_results = IntCounterVec::new(
            Opts::new(
                "verify_image_results_total",
                "number of image verified per resource",
            ),
            &["name", "result"],
        )?;
        let update_calls = IntCounterVec::new(
            Opts::new(
                "update_calls_total",
                "number of updates triggered per resource",
            ),
            &["name"],
        )?;
        let kubeapi_calls = IntCounter::with_opts(Opts::new(
            "kubeapi_calls_total",
            "number of kubernetes master server api in total",
        ))?;
        let registry_requests = IntCounterVec::new(
            Opts::new(
                "registry_requests_total",
                "number of api calls to registry per registry",
            ),
            &["registry"],
        )?;

        registry.register(Box::new(checks_started.clone()))?;
        registry.register(Box::new(checks_finished.clone()))?;
        registry.register(Box::new(verify_image_checks.clone()))?;
        registry.register(Box::new(verify_image_results.clone()))?;
        registry.register(Box::new(update_calls.clone()))?;
        registry.register(Box::new(kubeapi_calls.clone()))?;
        registry.register(Box::new(registry_requests.clone()))?;

        Ok(Self {
            checks_started,
            checks_finished,
            verify_image_checks,
            verify_image_results,
            update_calls,
            kubeapi_calls,
            registry_requests,
        })
    }
}
