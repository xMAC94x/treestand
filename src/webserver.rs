use http_body_util::Full;
use hyper::{body::Bytes, header, service::Service, Request, Response, StatusCode};
use hyper_util::rt::TokioIo;
use prometheus::{Encoder, Registry, TextEncoder};
use std::{
    convert::Infallible,
    future::Future,
    net::SocketAddr,
    pin::Pin,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
};
use tokio::net::TcpListener;
use tracing::{info, trace};

#[derive(Clone)]
pub struct Server {
    registry: Arc<Registry>,
    pub readiness: Arc<AtomicBool>,
}

impl Server {
    pub fn new(registry: Arc<Registry>) -> Self {
        Self {
            registry,
            readiness: Arc::new(AtomicBool::new(false)),
        }
    }

    pub async fn run<F>(self, addr: SocketAddr, shutdown: F) -> Result<(), std::io::Error>
    where
        F: Future<Output = ()>,
    {
        info!(?addr, "starting hyper server to serve metrics");

        let listener = TcpListener::bind(addr).await?;
        let mut shutdown = core::pin::pin!(shutdown);

        while let Some(conn) = tokio::select! {
            _ = shutdown.as_mut() => None,
            conn = listener.accept() => Some(conn),
        } {
            match conn {
                Ok((tcp, _)) => {
                    let io = TokioIo::new(tcp);
                    let service_clone = self.clone();

                    tokio::task::spawn(async move {
                        use hyper::server::conn::http1;
                        let conn = http1::Builder::new().serve_connection(io, service_clone);

                        if let Err(e) = conn.await {
                            tracing::error!(?e, "error serving connection")
                        }
                    });
                },
                Err(e) => tracing::error!(?e, "error accepting new connection"),
            }
        }

        Ok(())
    }
}

impl Service<Request<hyper::body::Incoming>> for Server {
    type Error = Infallible;
    type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>> + Send>>;
    type Response = Response<Full<Bytes>>;

    fn call(&self, req: Request<hyper::body::Incoming>) -> Self::Future {
        let uri = req.uri();
        trace!(?uri, "request");

        let (code, body) = match uri.path() {
            "/metrics" => {
                let mf = self.registry.gather();
                let mut buffer = vec![];
                let encoder = TextEncoder::new();

                encoder
                    .encode(&mf, &mut buffer)
                    .expect("write to vec cannot fail");
                let buffer: Bytes = buffer.into();
                (StatusCode::OK, Full::new(buffer))
            },
            "/readiness" => {
                if self.readiness.load(Ordering::SeqCst) {
                    (StatusCode::OK, Full::new(Bytes::new()))
                } else {
                    (StatusCode::SERVICE_UNAVAILABLE, Full::new(Bytes::new()))
                }
            },
            _ => {
                trace!("wrong uri, return 404");
                let buffer = Bytes::from_static(
                    "404 not found - available are /metrics and /readiness".as_bytes(),
                );
                (StatusCode::NOT_FOUND, Full::new(buffer))
            },
        };

        let response = Response::builder()
            .status(code)
            .header(header::CONTENT_TYPE, "text/plain; charset=utf-8")
            .body(body)
            .unwrap();

        Box::pin(async { Ok::<Response<http_body_util::Full<Bytes>>, Infallible>(response) })
    }
}
