use crate::{
    kubeapi::{K8sResourceId, Kubeapi},
    metrics::ResourceMetrics,
    registry::Registry,
    registry_payloads::ManifestHeaders,
    settings::{Entry, UpdateStrategy, VerifyStrategy},
    utils::Image,
};
use chrono::{DateTime, Utc};
use std::{sync::Arc, time::Duration};
use tracing::{debug, error, info, instrument, trace, warn};

#[derive(Debug, Clone)]
pub struct Resource {
    pub config: Entry,
    pub last_update: DateTime<Utc>,
    pub registry: Registry,
    pub kubeapi: Kubeapi,
    metrics: Arc<ResourceMetrics>,
}

impl Resource {
    pub async fn new(
        config: Entry,
        metrics: &Arc<ResourceMetrics>,
    ) -> Result<Self, Box<dyn std::error::Error>> {
        let last_update = Utc::now();
        let registry = Registry::new(Arc::clone(metrics));
        let kubeapi = Kubeapi::new(Arc::clone(metrics)).await?;
        Ok(Self {
            config,
            last_update,
            registry,
            kubeapi,
            metrics: Arc::clone(metrics),
        })
    }

    #[instrument(name="run", skip(self), fields(name = %self.config.name))]
    pub async fn endless_run(mut self) {
        trace!("Starting watch on resource");
        loop {
            let _ = self.check().await;
        }
    }

    pub async fn check(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        self.metrics
            .checks_started
            .with_label_values(&[&self.config.name])
            .inc();
        let filter = self.config.resourceFilter.clone();
        tokio::time::sleep(self.config.check_delay + Duration::from_secs(1)).await;
        let resources = match self.kubeapi.filter(filter).await {
            Ok(f) => f,
            Err(e) => {
                warn!(?e, "filter failed");
                self.metrics
                    .checks_finished
                    .with_label_values(&[&self.config.name, "false"])
                    .inc();
                return Err(e);
            },
        };

        for res in resources {
            if let Err(e) = self.check_res(res).await {
                warn!(?e, "check failed");
                self.metrics
                    .checks_finished
                    .with_label_values(&[&self.config.name, "false"])
                    .inc();
                return Err(e);
            }
        }
        self.metrics
            .checks_finished
            .with_label_values(&[&self.config.name, "true"])
            .inc();

        Ok(())
    }

    #[instrument(name = "check", skip(self))]
    pub async fn check_res(
        &mut self,
        res: K8sResourceId,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let images = self
            .kubeapi
            .get_images(&res, &self.config.container_name)
            .await?;
        if images.is_empty() {
            debug!("images is empty");
        }

        for image in images {
            // get respective image
            trace!(?image, "going to check online for a newer version");
            let image = Image::new(&image).ok_or("couldn't retrieve Image")?;
            let (manifest, manifest_headers) = self.registry.manifest(image.clone()).await?;
            trace!(?manifest, ?manifest_headers, "manifest");

            {
                let sha = &manifest_headers.docker_content_digest;
                let created = manifest_headers.date;
                trace!(?sha, ?created, "available image info");
            }
            // verify
            trace!("verify the resource");
            if self.verify(&manifest_headers, &image, &res).await? {
                trace!(
                    "update will be done in {} seconds",
                    self.config.delay_before_update.as_secs()
                );
                tokio::time::sleep(self.config.delay_before_update).await;
                self.update(&res).await?;
                trace!(
                    "sleeping for {} seconds",
                    self.config.delay_after_update.as_secs()
                );
                tokio::time::sleep(self.config.delay_after_update).await;
                break;
            }
        }
        Ok(())
    }

    #[instrument(name = "verify", skip(self, res, manifest_headers))]
    async fn verify(
        &self,
        manifest_headers: &ManifestHeaders,
        image: &Image,
        res: &K8sResourceId,
    ) -> Result<bool, Box<dyn std::error::Error>> {
        // verify
        self.metrics
            .verify_image_checks
            .with_label_values(&[&self.config.name])
            .inc();
        let config = self.config.clone();
        let kubeapi = self.kubeapi.clone();
        let last_update = self.last_update;
        let r = async move {
            match self.config.verifyStrategy {
                VerifyStrategy::Always => {
                    info!("verify passed: always is set");
                    Ok(true)
                },
                VerifyStrategy::Time => {
                    let age = manifest_headers.date.signed_duration_since(last_update);
                    if age < chrono::Duration::from_std(Duration::from_secs(0)).unwrap() {
                        debug!(
                            ?age,
                            "Our local image is more up-to-date then remote, skipping"
                        );
                        return Ok(false);
                    }
                    info!(?age, "verify passed: Detected a newer online image");
                    Ok(true)
                },
                VerifyStrategy::Hash => {
                    let pod_images = kubeapi
                        .get_pod_image_ids(res, &config.container_name)
                        .await?;
                    // get the shas
                    let mut pod_image_shas = vec![];
                    for image in &pod_images {
                        if let Some(sha) = image.rsplit_once('@').map(|(_, a)| a.to_owned()) {
                            pod_image_shas.push(sha);
                        }
                    }
                    let remote = &manifest_headers.docker_content_digest;
                    if pod_image_shas.iter().any(|e| e != remote) {
                        info!(?remote, ?pod_images, "found image that differs from remote");
                        Ok(true)
                    } else {
                        debug!(?remote, ?pod_images, "all images are up to date");
                        Ok(false)
                    }
                },
            }
        }
        .await;
        let metric = &self.metrics.verify_image_results;
        match &r {
            Ok(true) => metric
                .with_label_values(&[&self.config.name, "Ok(true)"])
                .inc(),
            Ok(false) => metric
                .with_label_values(&[&self.config.name, "Ok(false)"])
                .inc(),
            Err(_) => metric
                .with_label_values(&[&self.config.name, "Err()"])
                .inc(),
        };
        r
    }

    #[instrument(name = "update", skip(self, res))]
    async fn update(&mut self, res: &K8sResourceId) -> Result<(), Box<dyn std::error::Error>> {
        //update
        self.metrics
            .update_calls
            .with_label_values(&[&self.config.name])
            .inc();
        match self.config.updateStrategy {
            UpdateStrategy::Simulate => info!("simulate only, do nothing"),
            UpdateStrategy::Rollout => match self.kubeapi.rollout(res).await {
                Ok(()) => {
                    info!("rollout successful");
                },
                Err(e) => {
                    error!(?e, "failed to restart resource");
                    return Err(e);
                },
            },
        }
        self.last_update = Utc::now();
        Ok(())
    }
}
