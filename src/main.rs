use tokio::runtime::Runtime;

mod kubeapi;
mod metrics;
mod parameters;
mod registry;
mod registry_payloads;
mod resource;
mod settings;
mod utils;
mod webserver;

use clap::Parser;
use parameters::Opts;
use resource::Resource;
use settings::Settings;
use std::{
    net::{IpAddr, Ipv4Addr, SocketAddr},
    path::Path,
    sync::Arc,
};
use tracing::{debug, error, info, trace, Level};
use tracing_subscriber::filter::EnvFilter;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let inittrace = tracing_subscriber::FmtSubscriber::builder()
        .with_max_level(Level::TRACE)
        .with_env_filter(EnvFilter::from_default_env())
        .finish();
    let (opts, settings) = tracing::subscriber::with_default(inittrace, || {
        let opts: Opts = Opts::parse();
        let path = &opts.config;
        info!(?path, "trying to load settings");
        let settings = Settings::load(Path::new(path));
        debug!(?settings, "setting initialized");
        (opts, settings)
    });
    let settings = settings?;

    let append_info = |mut f: EnvFilter, list: &[&str], level: &str| {
        for l in list {
            f = f.add_directive(format!("{}={}", l, level).parse().unwrap());
        }
        f
    };

    let list = &[
        "hyper",
        "trust_dns_resolver",
        "trust_dns_proto",
        "reqwest",
        "mio",
        "want",
        "kube",
        "tower",
    ];

    let filter = EnvFilter::from_default_env();
    let filter = match opts.verbose {
        0 => append_info(filter.add_directive(Level::INFO.into()), list, "info"),
        1 => append_info(filter.add_directive(Level::DEBUG.into()), list, "info"),
        2 => append_info(filter.add_directive(Level::DEBUG.into()), list, "debug"),
        3 => append_info(filter.add_directive(Level::TRACE.into()), list, "info"),
        4 => append_info(filter.add_directive(Level::TRACE.into()), list, "debug"),
        _ => append_info(filter.add_directive(Level::TRACE.into()), list, "trace"),
    };

    tracing_subscriber::FmtSubscriber::builder()
        .with_max_level(Level::TRACE)
        .with_env_filter(filter)
        .try_init()
        .unwrap();

    debug!("tracing initialized");
    let rt = Runtime::new().expect("couln't start runtime");
    debug!("runtime initialized");

    if opts.skip_self_check {
        info!("Self check skipped");
    } else {
        match rt.block_on(initial_check(&settings)) {
            Err(e) => {
                error!(?e, "Self check failed");
                return Err(e);
            },
            Ok(()) => info!("Self check passed :)"),
        }
    }

    let registry = Arc::new(prometheus::Registry::new());
    let resource_metrics = Arc::new(metrics::ResourceMetrics::new(&registry)?);
    let readiness = if !opts.no_server {
        let server = webserver::Server::new(Arc::clone(&registry));
        let readiness = Arc::clone(&server.readiness);
        let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0)), opts.port);
        rt.spawn(server.run(addr, futures_util::future::pending()));
        trace!("HTTP server triggered");
        readiness
    } else {
        Arc::new(std::sync::atomic::AtomicBool::new(false))
    };

    let mut handles = Vec::new();
    for s in settings.entries.iter() {
        let mut r = rt.block_on(Resource::new(s.clone(), &resource_metrics))?;
        handles.push(if opts.run_once {
            rt.spawn(async move {
                let _ = r.check().await;
            })
        } else {
            rt.spawn(r.endless_run())
        });
    }

    info!("Started Treestand");
    readiness.store(true, std::sync::atomic::Ordering::SeqCst);

    for h in handles {
        match rt.block_on(h) {
            Ok(()) => trace!("handle finished"),
            Err(e) => error!(?e, "error in resource execution"),
        }
    }
    info!("Stopped gracefully");
    Ok(())
}

/// Before running in production, verify as much as we can.
async fn initial_check(settings: &Settings) -> Result<(), Box<dyn std::error::Error>> {
    trace!("starting selfcheck");
    let registry = Arc::new(prometheus::Registry::new());
    let resource_metrics = Arc::new(metrics::ResourceMetrics::new(&registry)?);

    let client = match kube::Client::try_default().await {
        Err(e) => {
            return Err(format!(
                "Failed to reach the kubernetes api, do we have correct settings ?: {}",
                e
            )
            .into());
        },
        Ok(c) => c,
    };
    let info = client.apiserver_version().await?;
    trace!(?info, "kubernetes server api");
    if settings.entries.is_empty() {
        return Err("no entries configured".into());
    }
    for entry in &settings.entries {
        let mut entry = entry.clone();
        use std::time::Duration;
        entry.check_delay = Duration::from_secs(0);
        entry.delay_before_update = Duration::from_secs(0);
        entry.delay_after_update = Duration::from_secs(0);
        entry.updateStrategy = settings::UpdateStrategy::Simulate;
        let mut r = Resource::new(entry, &resource_metrics).await?;
        r.check().await?;
    }
    Ok(())
}
