#![allow(non_snake_case)]

use serde::{Deserialize, Serialize};
use std::{fs, path::Path};
use tracing::{error, warn};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Settings {
    pub entries: Vec<Entry>,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum Kind {
    Deployment,
    StatefulSet,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Filter {
    ResourceFilter {
        kind: Kind,
        namespace: String,
        selector: Option<String>,
        field_selector: Option<String>,
    },
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum VerifyStrategy {
    /// update always
    Always,
    /// update when remote image is newer than local image
    Time,
    /// update when remote image differ in hash with local image
    Hash,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum UpdateStrategy {
    /// don't do anything
    Simulate,
    /// kubectl rollout
    Rollout,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Entry {
    /// free text field used for logging
    pub name: String,
    /// used to authenticate towards the container registry
    pub username: String,
    /// The filter is used to get Kind and Name of our resource to watch and
    /// modify
    pub resourceFilter: Filter,
    /// ContainerName with the image inside the Pod
    pub container_name: Option<String>,
    /// Delay between 2 checks to detect resources, to detect images
    pub check_delay: std::time::Duration,
    /// Delay After noticed a newer image before updating the resource
    pub delay_before_update: std::time::Duration,
    /// Delay after change the resource before doing another check, e.g. to
    /// limit update to once a day.
    pub delay_after_update: std::time::Duration,
    /// choose how different image should be detected
    pub verifyStrategy: VerifyStrategy,
    /// choose what to do after a newer image was detected
    pub updateStrategy: UpdateStrategy,
}

impl Default for Settings {
    fn default() -> Self {
        let entry = Entry {
            name: "example_selfupdate".to_owned(),
            username: "test".to_owned(),
            resourceFilter: Filter::ResourceFilter {
                kind: Kind::Deployment,
                namespace: "default".to_owned(),
                selector: None,
                field_selector: Some("metadata.name=veloren-treestand".to_owned()),
            },
            container_name: None,
            check_delay: std::time::Duration::from_secs(60),
            delay_before_update: std::time::Duration::from_secs(30),
            delay_after_update: std::time::Duration::from_secs(300),
            verifyStrategy: VerifyStrategy::Time,
            updateStrategy: UpdateStrategy::Rollout,
        };
        Self {
            entries: vec![entry],
        }
    }
}

impl std::fmt::Display for Kind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Kind::Deployment => write!(f, "Deployment"),
            Kind::StatefulSet => write!(f, "StatefulSet"),
        }
    }
}

impl std::fmt::Display for Filter {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Filter::ResourceFilter {
                selector,
                field_selector,
                kind,
                namespace,
            } => write!(f, "({} n={}", kind, namespace)
                .and_then(|()| {
                    if let Some(selector) = selector {
                        write!(f, ", sel={}", selector)
                    } else {
                        Ok(())
                    }
                })
                .and_then(|()| {
                    if let Some(field_selector) = field_selector {
                        write!(f, ", fsel={}", field_selector)
                    } else {
                        Ok(())
                    }
                })
                .and_then(|()| write!(f, ")")),
        }
    }
}

impl Settings {
    /// path: /opt/treestand/config/settings.ron
    pub fn load(path: &Path) -> Result<Self, Box<dyn std::error::Error>> {
        let file = fs::File::open(path)?;
        match ron::de::from_reader(file) {
            Ok(x) => Ok(x),
            Err(e) => {
                let default_settings = Self::default();
                let template_path = path.with_extension("template.ron");
                warn!(
                    ?e,
                    "Failed to parse setting file! Falling back to default settings and creating \
                     a template file for you to migrate your current settings file: {}",
                    template_path.display()
                );
                if let Err(e) = default_settings.save_to_file(&template_path) {
                    error!(?e, "Failed to create template settings file")
                }
                Err(e.into())
            },
        }
    }

    fn save_to_file(&self, path: &Path) -> std::io::Result<()> {
        // Create dir if it doesn't exist
        if let Some(dir) = path.parent() {
            fs::create_dir_all(dir)?;
        }
        let ron = ron::ser::to_string_pretty(self, ron::ser::PrettyConfig::default())
            .expect("Failed serialize settings.");

        fs::write(path, ron.as_bytes())?;

        Ok(())
    }
}
