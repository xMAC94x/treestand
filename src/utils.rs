#[derive(Debug, Clone)]
pub struct Image {
    pub registry: String,
    pub name: String,
    pub tag: String,
}

impl Image {
    pub fn new(image: &str) -> Option<Self> {
        image.split_once('/').and_then(|(registry, rest)| {
            rest.rsplit_once(':').map(|(name, tag)| Self {
                registry: registry.to_owned(),
                name: name.to_owned(),
                tag: tag.to_owned(),
            })
        })
    }

    #[allow(dead_code)]
    pub fn full(&self) -> String { format!("{}/{}:{}", self.registry, self.name, self.tag) }
}

#[derive(Debug)]
pub enum WwwAuthenticate {
    Bearer {
        realm: String,
        service: String,
        scope: String,
    },
}

impl WwwAuthenticate {
    pub fn new(header: &str) -> Option<Self> {
        use regex::Regex;
        let re = Regex::new(r#"Bearer realm="(.*)",service="(.*)",scope="(.*)""#)
            .expect("constant regex should be valid");

        if let Some(caps) = re.captures(header) {
            if let (Some(realm), Some(service), Some(scope)) =
                (caps.get(1), caps.get(2), caps.get(3))
            {
                let realm = realm.as_str().to_owned();
                let service = service.as_str().to_owned();
                let scope = scope.as_str().to_owned();
                Some(Self::Bearer {
                    realm,
                    service,
                    scope,
                })
            } else {
                None
            }
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn image() {
        let uri = "registry.gitlab.com/veloren/veloren/server-cli:nightly";
        let image = Image::new(uri).unwrap();
        assert_eq!(image.registry, "registry.gitlab.com".to_owned());
        assert_eq!(image.name, "veloren/veloren/server-cli".to_owned());
        assert_eq!(image.tag, "nightly".to_owned());
        assert_eq!(&image.full(), uri);
    }
}
