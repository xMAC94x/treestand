use crate::{
    metrics::ResourceMetrics,
    registry_payloads::*,
    utils::{Image, WwwAuthenticate},
};
use hyper::header;
use reqwest::Client;
use std::{
    hash::{Hash, Hasher},
    sync::Arc,
};
use tracing::trace;

#[derive(Debug, Clone)]
pub struct Registry {
    client: Client,
    metrics: Arc<ResourceMetrics>,
}

impl Registry {
    pub fn new(metrics: Arc<ResourceMetrics>) -> Self {
        Self {
            client: Client::new(),
            metrics,
        }
    }

    async fn get_token(&self, www: WwwAuthenticate) -> Result<String, Box<dyn std::error::Error>> {
        // 'https://gitlab.com/jwt/auth?account=test&scope=repository%3Aveloren%2Fveloren%2Fserver-cli%3Apull&service=container_registry'
        //
        match www {
            WwwAuthenticate::Bearer {
                scope,
                service,
                realm,
            } => {
                let account = "test";
                let url = format!(
                    "{}?account={}&scope={}&service={}",
                    realm, account, scope, service
                );
                let resp = self.client.get(url).send().await?;

                let status = resp.status();
                let bytes = resp.bytes().await?;

                if status.is_success() {
                    let x: TokenRequestPayload = serde_json::from_slice(&bytes)?;
                    Ok(x.token)
                } else {
                    Err(format!("couldn't get token: {}", status.as_str()).into())
                }
            },
        }
    }

    pub async fn manifest(
        &self,
        image: Image,
    ) -> Result<(ManifestPayload, ManifestHeaders), Box<dyn std::error::Error>> {
        self.metrics
            .registry_requests
            .with_label_values(&[&image.registry])
            .inc();
        // try to get the manifest
        let url = format!(
            "https://{}/v2/{}/manifests/{}",
            image.registry, image.name, image.tag
        );
        // https://registry.gitlab.com/v2/veloren/veloren/server-cli/manifests/nightly
        let resp = self.client.get(&url).send().await?;

        // expect 401 unauthorized
        let token = match resp.status().as_u16() {
            401 => {
                let www = resp
                    .headers()
                    .get("Www-Authenticate")
                    .ok_or("no Www-Authenticate header set")?;
                let www =
                    WwwAuthenticate::new(www.to_str()?).ok_or("Www-Authenticate parse failed")?;

                let token = self.get_token(www).await?;
                let mut token_hashed = std::hash::DefaultHasher::new();
                token.hash(&mut token_hashed);
                let token_start = &token[..token.len().min(4)];
                let token_hash = token_hashed.finish();
                trace!(?token_hash, ?token_start, "Token");
                token
            },
            e => return Err(format!("can only handle gitlab registry atm: {}", e).into()),
        };

        const ENCODING: &str = "application/vnd.oci.image.manifest.v1+json";
        let resp = self
            .client
            .get(&url)
            .bearer_auth(token)
            .header(header::ACCEPT, ENCODING)
            .send()
            .await?;

        let status = resp.status();
        let headers = resp.headers();
        tracing::trace!(?status, ?headers, "manifest headers");
        if !status.is_success() {
            let error_mgs = resp.bytes().await?;
            let error_mgs = String::from_utf8_lossy(&error_mgs);
            return Err(format!("Error {status} fetching manifest: {error_mgs}").into());
        }

        let header = ManifestHeaders {
            docker_content_digest: headers
                .get("docker-content-digest")
                .ok_or("no digest in header")?
                .to_str()?
                .to_owned(),
            date: chrono::DateTime::parse_from_rfc2822(
                headers.get("date").ok_or("no date in header")?.to_str()?,
            )?,
        };

        let bytes = resp.bytes().await?;
        tracing::trace!(?bytes, "manifest Bytes");

        if status.is_success() {
            let manifest: ManifestPayload = serde_json::from_slice(&bytes)?;
            Ok((manifest, header))
        } else {
            Err(format!("couldn't get manifest: {}", status.as_str()).into())
        }
    }

    #[allow(dead_code)]
    pub async fn get_blob(
        &self,
        image: Image,
        digest: String,
    ) -> Result<BlobPayload, Box<dyn std::error::Error>> {
        self.metrics
            .registry_requests
            .with_label_values(&[&image.registry])
            .inc();
        // try to get the blob
        let url = format!(
            "https://{}/v2/{}/blobs/{}",
            image.registry, image.name, digest
        );
        let resp = self.client.get(&url).send().await?;
        // expect 401 unauthorized

        match resp.status().as_u16() {
            401 => {
                let www = resp
                    .headers()
                    .get("Www-Authenticate")
                    .ok_or("no Www-Authenticate header set")?;
                let www =
                    WwwAuthenticate::new(www.to_str()?).ok_or("Www-Authenticate parse failed")?;

                let token = self.get_token(www).await?;
                trace!(?token, "Token");
                let resp = self.client.get(&url).bearer_auth(token).send().await?;

                let status = resp.status();
                let bytes = resp.bytes().await?;

                tracing::trace!(?bytes, "blob Bytes");

                if status.is_success() {
                    let blob: BlobPayload = serde_json::from_slice(&bytes)
                        .map_err(|e| format!("json error {:?}", e))?;
                    Ok(blob)
                } else {
                    Err(format!("couldn't get blob: {}", status.as_str()).into())
                }
            },
            e => Err(format!("can only handle gitlab registry atm: {}", e).into()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_gitlab() {
        let pr = prometheus::Registry::new();
        let metrics = ResourceMetrics::new(&pr).unwrap();
        let registry = Registry::new(Arc::new(metrics));
        let image = Image::new("registry.gitlab.com/veloren/veloren/server-cli:nightly").unwrap();
        let (_, header) = registry.manifest(image.clone()).await.unwrap();
        println!("Image: {}", header.docker_content_digest);
        assert!(header.docker_content_digest.starts_with("sha256:"));
    }
}
