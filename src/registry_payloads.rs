#![allow(non_snake_case)]

use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TokenRequestPayload {
    pub token: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ManifestLayerMeta {
    pub mediaType: String,
    pub size: u64,
    pub digest: String,
    pub urls: Option<Vec<String>>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ManifestPayload {
    pub schemaVersion: u64,
    pub mediaType: String,
    pub config: ManifestLayerMeta,
    pub layers: Vec<ManifestLayerMeta>,
}

#[derive(Debug)]
pub struct ManifestHeaders {
    pub docker_content_digest: String,
    pub date: DateTime<chrono::FixedOffset>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BlobHistory {
    pub created: DateTime<Utc>,
    pub created_by: String,
    #[serde(default)]
    pub empty_layer: bool,
    pub author: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BlobRootFs {
    #[serde(alias = "type")]
    pub type_: String,
    pub diff_ids: Vec<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BlobConfig {
    pub Cmd: Vec<String>,
    pub Entrypoint: Option<Vec<String>>,
    pub Env: Vec<String>,
    pub Image: String,
    pub WorkingDir: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BlobPayload {
    pub architecture: String,
    pub container: String,
    pub created: DateTime<Utc>,
    pub docker_version: String,
    pub os: String,
    pub history: Vec<BlobHistory>,
    pub rootfs: BlobRootFs,
    pub config: BlobConfig,
}
