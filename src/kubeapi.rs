use crate::{
    metrics::ResourceMetrics,
    settings::{Filter, Kind},
};
use k8s_openapi::api::{
    apps::v1::{Deployment, StatefulSet},
    core::v1::Pod,
};
use kube::{
    api::{Api, ListParams, Patch, PatchParams, ResourceExt},
    Client,
};
use std::sync::Arc;
use tracing::{debug, trace};

#[derive(Clone)]
pub struct Kubeapi {
    client: Client,
    metrics: Arc<ResourceMetrics>,
}

pub struct K8sResourceId {
    namespace: String,
    name: String,
    kind: Kind,
}

impl Kubeapi {
    pub async fn new(metrics: Arc<ResourceMetrics>) -> kube::Result<Self> {
        Ok(Self {
            client: Client::try_default().await?,
            metrics,
        })
    }

    pub async fn filter(
        &self,
        filter: Filter,
    ) -> Result<Vec<K8sResourceId>, Box<dyn std::error::Error>> {
        let mut resources = vec![];
        match &filter {
            Filter::ResourceFilter {
                field_selector,
                kind,
                namespace,
                selector,
            } => {
                // Start a watch call for pods matching our name
                let lp = kube::api::ListParams {
                    field_selector: field_selector.clone(),
                    label_selector: selector.clone(),
                    ..Default::default()
                };

                self.metrics.kubeapi_calls.inc();

                match kind {
                    Kind::Deployment => {
                        let deployments: Api<Deployment> =
                            Api::namespaced(self.client.clone(), namespace);

                        let deployments = deployments.list(&lp).await?;
                        for d in deployments {
                            let r = K8sResourceId {
                                name: d.name_any().to_owned(),
                                kind: kind.to_owned(),
                                namespace: namespace.to_owned(),
                            };
                            resources.push(r);
                        }
                    },
                    Kind::StatefulSet => {
                        let statefulsets: Api<StatefulSet> =
                            Api::namespaced(self.client.clone(), namespace);

                        let statefulsets = statefulsets.list(&lp).await?;
                        for s in statefulsets {
                            let r = K8sResourceId {
                                name: s.name_any().to_owned(),
                                kind: kind.to_owned(),
                                namespace: namespace.to_owned(),
                            };
                            resources.push(r);
                        }
                    },
                }
            },
        };

        let n = resources.len();
        debug!(?n, "detected n resources");
        trace!(?resources, "full list of all resources");

        Ok(resources)
    }

    pub async fn get_images(
        &self,
        resources: &K8sResourceId,
        container_name: &Option<String>,
    ) -> Result<Vec<String>, Box<dyn std::error::Error>> {
        let mut images = vec![];
        match resources.kind {
            Kind::Deployment => {
                let deployments: Api<Deployment> =
                    Api::namespaced(self.client.clone(), &resources.namespace);
                let deployment = deployments.get(&resources.name).await?;
                let podspec = deployment
                    .spec
                    .ok_or("No deployment spec")?
                    .template
                    .spec
                    .ok_or("No deployment template spec")?;
                for c in podspec.containers {
                    if let Some(image) = c.image {
                        if container_name.is_none() || container_name.clone().unwrap() == c.name {
                            images.push(image)
                        }
                    }
                }
            },
            Kind::StatefulSet => {
                let statefulsets: Api<StatefulSet> =
                    Api::namespaced(self.client.clone(), &resources.namespace);

                let statefulset = statefulsets.get(&resources.name).await?;
                let podspec = statefulset
                    .spec
                    .ok_or("No statefulset spec")?
                    .template
                    .spec
                    .ok_or("No statefulset template spec")?;
                for c in podspec.containers {
                    if let Some(image) = c.image {
                        if container_name.is_none() || container_name.clone().unwrap() == c.name {
                            images.push(image)
                        }
                    }
                }
            },
        }
        Ok(images)
    }

    /// Get the actual deployed pod images of our deployment
    pub async fn get_pod_image_ids(
        &self,
        resources: &K8sResourceId,
        container_name: &Option<String>,
    ) -> Result<Vec<String>, Box<dyn std::error::Error>> {
        let mut images = vec![];
        // get the metadata labels to find respective Pods to our deployment
        let podlabels = match resources.kind {
            Kind::Deployment => {
                let deployments: Api<Deployment> =
                    Api::namespaced(self.client.clone(), &resources.namespace);
                let deployment = deployments.get(&resources.name).await?;
                let podlabels = deployment
                    .spec
                    .ok_or("No deployment spec")?
                    .template
                    .metadata
                    .ok_or("No deployment template spec")?
                    .labels
                    .ok_or("deployment has no labels")?;
                labels_to_string(podlabels)
            },
            Kind::StatefulSet => {
                let statefulsets: Api<StatefulSet> =
                    Api::namespaced(self.client.clone(), &resources.namespace);

                let statefulset = statefulsets.get(&resources.name).await?;
                let podlabels = statefulset
                    .spec
                    .ok_or("No deployment spec")?
                    .template
                    .metadata
                    .ok_or("No deployment template spec")?
                    .labels
                    .ok_or("deployment has no labels")?;
                labels_to_string(podlabels)
            },
        };

        let lp = ListParams::default().labels(&podlabels);

        let pods: Api<Pod> = Api::namespaced(self.client.clone(), &resources.namespace);
        let pods = pods.list(&lp).await?;
        for p in pods {
            let status = p.status.ok_or("No pod status")?;
            if let Some("Failed") = status.phase.as_deref() {
                let name = p.metadata.name;
                debug!(?name, "skipping checking pod as it's failed");
                continue;
            }
            for c in status.container_statuses.ok_or("no container status")? {
                if container_name.is_none() || container_name.clone().unwrap() == c.name {
                    images.push(c.image_id)
                }
            }
        }

        Ok(images)
    }

    pub async fn rollout(
        &mut self,
        resources: &K8sResourceId,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let kind = resources.kind.to_string();
        use std::ops::Add;
        let time = chrono::Utc::now()
            .add(chrono::Duration::seconds(4))
            .to_rfc3339();
        let patch = serde_json::json!({
            "apiVersion": "apps/v1",
            "kind": kind,
            "spec": {
                "template": {
                    "metadata": {
                        "annotations": {
                            "kubectl.kubernetes.io/restartedAt": time
                        }
                    }
                }
            }
        });

        match resources.kind {
            Kind::Deployment => {
                let deployments: Api<Deployment> =
                    Api::namespaced(self.client.clone(), &resources.namespace);
                trace!(?patch, "going to patch deployment");
                let params = PatchParams::apply("treestand").force();
                let patch = Patch::Apply(&patch);
                deployments.patch(&resources.name, &params, &patch).await?;
            },
            Kind::StatefulSet => {
                let statefulsets: Api<StatefulSet> =
                    Api::namespaced(self.client.clone(), &resources.namespace);
                trace!(?patch, "going to patch statefulset");
                let params = PatchParams::apply("treestand");
                let patch = Patch::Apply(&patch);
                statefulsets.patch(&resources.name, &params, &patch).await?;
            },
        }
        Ok(())
    }
}

impl std::fmt::Debug for K8sResourceId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}@{}", self.name, self.namespace)
    }
}

impl std::fmt::Debug for Kubeapi {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result { write!(f, "Kubeapi") }
}

fn labels_to_string(labels: std::collections::BTreeMap<String, String>) -> String {
    let mut result = "".to_owned();
    for (u, (k, v)) in labels.iter().enumerate() {
        if u == 0 {
            result = format!("{}={}", k, v);
        } else {
            result = format!("{},{}={}", result, k, v);
        }
    }
    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_labels() {
        let mut kvmap = std::collections::BTreeMap::new();
        kvmap.insert("foo".to_owned(), "one".to_owned());
        kvmap.insert("key".to_owned(), "value".to_owned());
        let res = labels_to_string(kvmap);
        assert_eq!(res, "foo=one,key=value");
    }
}
