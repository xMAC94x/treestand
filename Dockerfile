FROM ubuntu:22.04 AS build

ARG RUST_TOOLCHAIN=nightly-2024-09-04

# Install deps
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive && \
    apt-get install -y --no-install-recommends --assume-yes \
        time \
        curl \
        ca-certificates \
        gcc \
        libc-dev \
        libssl-dev \
        pkg-config \
    # Cleanup extra cached files
    && rm -rf /var/lib/apt/lists/*;

RUN time curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --default-toolchain ${RUST_TOOLCHAIN} --profile=minimal

COPY . /data
WORKDIR /data

RUN . /root/.cargo/env \
    && cargo build --release --locked --all-features


# Copy the statically-linked binary into a scratch container.
FROM debian:stable-slim
RUN DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get install -y --no-install-recommends --assume-yes \
        ca-certificates \
        libssl3 \
    # Cleanup extra cached files
    && rm -rf /var/lib/apt/lists/* && \
    mkdir -p /opt/treestand/config/;
WORKDIR /opt/treestand/bin
COPY --from=build /data/target/release/treestand .
EXPOSE 8081
ENV RUST_LOG=trace
ENV RUST_BACKTRACE=full
CMD ["./treestand", "-vvv"]
